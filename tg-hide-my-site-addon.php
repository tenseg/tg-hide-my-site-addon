<?php
/**
 * Plugin Name: Hide My Site Exceptions Addon
 * Description: Adds the ability to exempt chosen pages or posts from the Hide My Site password requirement.
 * Version: 1.0.0
 * Author: Tenseg LLC
 * Author URI: http://tenseg.net
 * License: GPL2
 */

// we don't do anything unless we sense the Hide My Site plugin is active

if ( class_exists( 'hide_my_site' ) ) {

	/**
	 * Check our post metadata to see if this page
	 * should be open to the public.
	 *
	 * Called by action: hidemy_beforeverify
	 *
	 * @param hide_my_site $hidemy an instance of a class
	 * @return void
	 */
	function tg_hidemy_unprotect( $hidemy ) {
		global $post;

		if ( get_post_meta( $post->ID, 'tg_hidemy_unprotect', true ) ) {
			// hide_my_site class has an undocumented instance variable
			// which we are setting here so that it does not block this post
			$hidemy->open_to_public = true;
		}
	}
	add_action( 'hidemy_beforeverify', 'tg_hidemy_unprotect' );

	/**
	 * Adds the meta box with which the user can choose to
	 * make a post open to the public.
	 *
	 * Called by action: add_meta_boxes
	 *
	 * @return void
	 */
	function tg_hidemy_add_custom_box() {
		add_meta_box(
			'tg_hidemy_box_id', // Unique ID
			'Site Password', // Box title
			'tg_hidemy_custom_box_html', // Content callback, must be of type callable
			['post', 'page'],
			'side'
		);
	}
	add_action( 'add_meta_boxes', 'tg_hidemy_add_custom_box' );

	/**
	 * Renders the meta box which allows the user to choose
	 * to open a post to the public.
	 *
	 * @param WP_Post $post
	 * @return void
	 */
	function tg_hidemy_custom_box_html( $post ) {
		$selected = get_post_meta( $post->ID, 'tg_hidemy_unprotect', true ) === 'public' ? 'selected' : '';
		$post_type = get_post_type_object( $post->post_type );
		if ( $post_type ) {
			$singular = esc_html( $post_type->labels->singular_name );
			?>
			<select name="tg_hidemy_unprotect" id="tg_hidemy_unprotect">
				<option value="">Password Protected</option>
				<option value="public" <?php echo $selected; ?>>Open this <?php echo $singular; ?> to the Public</option>
			</select>
			<?php

		}
	}

	/**
	 * Saves meta data based on what is returned when a post is updated.
	 *
	 * Called by action: save_post
	 *
	 * @param int $post_id
	 * @return void
	 */
	function tg_hidemy_save_postdata( $post_id ) {
		if ( array_key_exists( 'tg_hidemy_unprotect', $_POST ) ) {
			update_post_meta(
				$post_id,
				'tg_hidemy_unprotect',
				$_POST['tg_hidemy_unprotect']
			);
		}
	}
	add_action( 'save_post', 'tg_hidemy_save_postdata' );

}