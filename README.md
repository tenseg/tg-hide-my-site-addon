# Hide My Site Exceptions Addon
Contributors: eceleste  
Donate link: https://www.tenseg.net  
Tags: hidemysite  
Requires at least: 5.4  
Tested up to: 5.4.2  
Stable tag: 1.0.0  
Requires PHP: 7.0

Adds the ability to exempt chosen pages or posts from the [Hide My Site](https://wordpress.org/plugins/hide-my-site/) password requirement.

## Description

Adds a "Site Password" meta box to all pages and posts which allows you to choose to open that page to the public instead of enforcing the site-wide password created with Hide My Site.

## Installation

Clone this repository into `/wp-content/plugins/` or upload the `tg-hide-my-site-addon` folder to the `/wp-content/plugins/` directory

Activate the plugin through the 'Plugins' menu in WordPress

## Changelog

### 1.0.0 (2021-02-06)
* initial version

## Developer Information

### Version Bumps

Please update the following files whenever you need to bump the version. Note, a version bump is essential to having changes in the stylesheet recognized by browsers in the wild.

* [tg-hide-my-site-addon.php](tg-hide-my-site-addon.php) (change the Version in the header, which is the usual WordPress thing to do)
* in this file replace the Stable tag in the header, add to the Changelog, and update the Upgrade Notice